package pe.edu.uni.fiis.matricula.controller;
import pe.edu.uni.fiis.matricula.model.*;
import pe.edu.uni.fiis.matricula.util.Reporte;

import java.util.List;
import java.util.ArrayList;

public class MatriculaController {
    public static void main(String[] args) {
        //create object
        Matricula matricula= new Matricula();
        matricula.setPrecio(1200f);

        matricula.setAlumno(new Alumno());
        matricula.getAlumno().setApellidos("Bendezu Isidro");
        matricula.getAlumno().setNombres("Franz Antony");
        matricula.getAlumno().setDni("72201518");

        matricula.setCuotas(new ArrayList<Cuota>());
        Cuota c1=new Cuota();
        c1.setCosto(400f);
        Cuota c2=new Cuota();
        c2.setCosto(500f);
        Cuota c3=new Cuota();
        c3.setCosto(600f);
        matricula.getCuotas().add(c1);
        matricula.getCuotas().add(c2);
        matricula.getCuotas().add(c3);

        Reporte.generarPdf(matricula);

    }
}
