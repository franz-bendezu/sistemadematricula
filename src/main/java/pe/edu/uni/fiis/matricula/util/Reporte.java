package pe.edu.uni.fiis.matricula.util;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.*;
import pe.edu.uni.fiis.matricula.model.Matricula;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class Reporte {
    public static void generarPdf(Matricula matricula){
        Document document=new Document();
        try {
            PdfWriter.getInstance(document,
                    new FileOutputStream(
                            "D:\\pdf\\matricula.pdf"));
            document.open();
            Paragraph titulo = new Paragraph("Boleta de Matricula",
                    FontFactory.getFont(FontFactory.HELVETICA, 22, Font.BOLD, new Color(0, 0, 0, 255)));
            titulo.setAlignment(Element.ALIGN_CENTER);
            document.add(titulo);
            document.add(new Paragraph(
                     "\n Nombre     :   "+ matricula.getAlumno().getNombres()) );
            document.add(new Paragraph(
                    "Apellido   :   "+ matricula.getAlumno().getApellidos()));
            document.add(new Paragraph(
                    "DNI        :   "+ matricula.getAlumno().getDni()));
            document.add(new Paragraph(
                    "Apellido   :   "));

            document.close();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
